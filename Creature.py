class Creature():

    def __init__(self, health, power, attackSpeed):
        self.health = int(health)
        self.maxHealth = int(health)
        self.power = int(power)
        self.attackSpeed = int(attackSpeed)
        self.go = False
        self.attributePoints = 0

    def changeAttackSpeed(self):
        self.attackSpeed -= 2

    def changeHealth(self):
        self.maxHealth += 15

    def changePower(self):
        self.power += 5

    def iscreaturedead(self):
        if self.health <= 0:
            return True
        else:
            return False

    def __str__(self):
        template = "Życie: {}, Szybkość ataku: {}, Siła: {}"
        return template.format(self.health, self.attackSpeed, self.power)
