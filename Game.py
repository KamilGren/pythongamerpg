import random

from Creature import Creature



class Game():

    def __init__(self):
        self.hero = Creature(150, 50, 15)

    def fight(self):
        self.hero.health = self.hero.maxHealth
        self.monster = Creature(self.hero.health * (random.randrange(5, 10, 1) / 10),
        self.hero.power * (random.randrange(5, 11, 2) / 10),
        self.hero.attackSpeed * (random.randrange(5, 20, 5) / 10))

        print("Trafiłeś na potwora! Statystyki potwora:")
        print(self.monster.__str__())
        print("Twój bohater: ")
        print(self.hero.__str__())

        for i in range(1000):
            if i % self.hero.attackSpeed == 0:
                self.monster.health -= self.hero.power / 2
                print("Przeprowadzasz atak zabierając potworowi: " + str(self.hero.power / 2) + " punktów życia!")
                if self.monster.iscreaturedead():
                    print("Udało Ci się zabić potwora. Gratulacje!")
                    print("Skończyłeś posiadając " + str(self.hero.health) + " punktów życia.")
                    print("Czy chcesz kontynuować rozgrywkę? 1 - TAK 2 - NIE")
                    k = input()
                    if k == "1":
                        print("Idziemy dalej!")
                        self.hero.attributePoints += 1
                        # czyli w tym konkretnym game zmieniamy atrybuty dla obiektu hero
                        self.setAttributes()
                        self.nextround()
                        break
                    if k == "2":
                        print("Zamykam rozgrywkę. Koniec gry.")
                        break
            if i % self.monster.attackSpeed == 0:
                self.hero.health -= self.monster.power / 2
                print("Potwór atakuje. Zabiera Ci " + str(self.monster.power / 2) + " punktów życia!")
                if self.hero.iscreaturedead():
                    print("Niestety ale potwór Cię zamordował. To koniec.")
                    print("Życie potwora: " + str(self.monster.health))
                    print("Możemy zacząć od nowa. Gotowy? 1 - TAK, 2 - NIE")
                    k = input()
                    if k == "1":
                        self.hero = Creature(150, 50, 15, True)
                        self.fight(self)
                        break
                    if k == "2":
                        print("Koniec gry.")
                    break

    def nextround(self):
        print(self.hero.__str__())
        self.fight()

    def setAttributes(self):
        self.hero.go = False
        while self.hero.attributePoints > 0 and self.hero.go == False:

            print("Masz punkty nauki do rozdania. Którą ze swoich statystyk chcesz wzmocnić?")
            print("1. Punkty życia (+15 <wciśnij 1>")
            print("2. Siła (+5 <wciśnij 2>")
            print("3. Szybkość ataku (+0.15s szybciej <wciśnij 3>")
            print("4. Wzmocnie później (Teraz nie rozdaję punktów nauki).")
            option = int(input("Choose: "))

            def addHealth():
                self.hero.changeHealth()
                self.hero.attributePoints -= 1
                print("Twoje życie wzrosło o 15! ")
                print("Pozostało " + str(self.hero.attributePoints) + " punktów nauki.")
                print("Twoje aktualne życie: " + str(self.hero.maxHealth)),

            def addPower():
                self.hero.changePower()
                self.hero.attributePoints -= 1
                print("Twoja siła wzrosła o 5! ")
                print("Pozostało " + str(self.hero.attributePoints) + " punktów nauki.")
                print("Twoja aktualna siła: " + str(self.hero.power)),

            def addAttackSpeed():
                self.hero.changeAttackSpeed()
                self.hero.attributePoints -= 1
                print("Twoja szybkość ataku wzrosła o 0,2s! ")
                print("Pozostało " + str(self.hero.attributePoints) + " punktów nauki.")
                print("Twoja aktualna szybkość ataku: " + str(self.hero.attackSpeed / 10) + "s "),

            def back():
                print("Przechodzę dalej bez rozdania punktów nauki!")
                self.hero.go = True

            switch = {
                1: addHealth,
                2: addPower,
                3: addAttackSpeed,
                4: back,
                13: back
            }
            switch.get(option)()


game = Game()
game.fight()

